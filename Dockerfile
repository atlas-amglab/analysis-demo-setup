FROM atlas/analysisbase:22.2.0
COPY --chown=atlas build/analysis.rpm /home/analysis/analysis.rpm
RUN sudo yum install -y /home/analysis/analysis.rpm && sudo rm /home/analysis/analysis.rpm
